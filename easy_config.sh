sudo apt update; sudo apt install tmux git;

git clone https://github.com/theo-l/tmux-powerline ~/.tmux-powerline


cat << EOF > ~/.tmux.conf
#remap prefix to Control+Space
unbind C-b
set -g prefix C-Space
bind Space send-prefix


#using Space to enter copy mode
bind Space copy-mode
bind C-Space copy-mode

#using vim navigate
setw -g mode-keys vi

#enable mouse in tmux
set -g mouse on


#tmux 256 color support for terminal for vim
set -g default-terminal "screen-256color"

#set tmux don't change the window name automaticlly
setw -g automatic-rename off

#pane navigation
bind-key j select-pane -D 
bind-key k select-pane -U 
bind-key h select-pane -L 
bind-key l select-pane -R 

#--------split window with <C-Space>| and <C-Space>-
#horizonal split
bind - split-window -v
#vertical split
bind | split-window -h
#--------------------------------------------------
#

#------------ colors----------------------------{
#

# 设置状态栏的颜色
set -g status-bg "colour33"
set -g status-fg "colour255"

#set the left part of the status line
#Session
set -g status on
set -g status-interval 2
#set -g status-utf8 on
set -g status-justify "centre"
set -g status-left-length 60
set -g status-right-length 90
set -g status-left "#(~/.tmux-powerline/powerline.sh left)"
set -g status-right "#(~/.tmux-powerline/powerline.sh right)"

#setw -g clock-mode-colour green
#setw -g clock-mode-style 24

#set the windows part of status line
#Windows
# 当前活动窗口的显示格式
setw -g window-status-current-format '[#[bold,fg=colour255, bg=colour24]#I #W]'
setw -g window-status-separator " "
# 非当前窗口的显示格式
setw -g window-status-format "[#[fg=colour255,bg=colour236]#I #W]"




#----------Window/Pane--------------------{
set -g base-index 1
set -g pane-base-index 1

set -g pane-border-style fg=colour235
set -g pane-active-border-style fg=colour240

EOF



