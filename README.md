# easy_tmux

This project is used to help people use tmux in an easy way

# Install
```bash
#1. Clone the project to your local  

git clone https://gitlab.com/theo-l/easy_tmux 
cd easy_tmux/
./easy_config.sh

```


# Usage

1. Start tmux with a new session **hello** `tmux new -s hello`

2. Create a new pane/window in session with `Ctrl+Space+ c`

3. Split a window in horizontal `Ctrl+Space+ -`, vertical `Ctrol+Space+ |`

4. Navigation between split windows `Ctrl+Space + h(left),j(down), k(up), l(right)`

5. Resize one splited window `Ctrl+Space+ z`

6. Rename current window `Ctrl+Space+,`, then type the name



